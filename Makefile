SHELL := /bin/bash
OCI_BIN := /usr/bin/podman
BUILD_OPTS := --build-arg='ROS2_DISTRO=rolling'
IMAGE_REG := quay.io
IMAGE_NAMESPACE := fedora-sig-robotics/ros2
IMAGE_TAG := rolling-cs9 
IMAGE := ${IMAGE_REG}/${IMAGE_NAMESPACE}:${IMAGE_TAG}

.PHONY: image/build
image/build:
	${OCI_BIN}  build ${BUILD_OPTS} -t ${IMAGE} .

.PHONY: image
image: image/build
